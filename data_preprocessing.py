#coding: utf8

import json
import random
import torch
import os
import re
import numpy as np
from torch.utils.data import Dataset
from utilities import offsets2iob, unicodeToAscii



class Vocab:
    """
    a two way dictionary between objects (words or chars mostly) and integers with convenient methods
    """
    def __init__(self, text = []):
        """
        initialize the vocabulary with the words in text.
        :param text: string list
        """
        self.odd_cases = {}
        self.to_ix = {}
        self.from_ix = []
        self.word_count = {}
        self.trimmed = False

        self.add_text(text)

    def add_text(self, text):
        """
        adds all the words in text to the vocabulary, except those that match an odd case
        :param text: string list
        """
        for word in text:
            if word not in self.from_ix:
                add = True
                for template, base in self.odd_cases.items():
                    if re.fullmatch(template, word):
                        add = False
                        self.word_count[base] += 1
                if add:
                    self.to_ix[word] = len(self.to_ix)
                    self.from_ix.append(word)
                    self.word_count[word] = 1
            else:
                self.word_count[word] += 1


    def add_odd_case(self, template, base):
        """
        all words that match the template will be treated as the word base
        :param template: a regular expression
        :param base: a word
        """
        self.odd_cases[template] = base
        self.add_text([base])

    def trim(self, threshold=5, exclude=[]):
        """
        removes words that appear less than threshold times in the wordcount
        :param threshold: int
        :param exclude: list of words to keep (usually <UNKNOWN>, <PAD>, etc)
        :return:
        """
        if not self.trimmed:
            to_ix = {word: i for i, word in enumerate(exclude)}
            from_ix = [word for word in exclude]
            word_count = {word: self.word_count[word] for word in exclude}
            for _, base in self.odd_cases.items():
                to_ix[base] = len(to_ix)
                from_ix.append(base)
            for word, count in self.word_count.items():
                if count > threshold and word not in exclude:
                    to_ix[word] = len(to_ix)
                    from_ix.append(word)
                    word_count[word] = self.word_count[word]
            self.to_ix = to_ix
            self.from_ix = from_ix
            self.word_count = word_count
            self.trimmed = True
        else:
            print("Already trimmed")


    def __len__(self):
        return len(self.to_ix)

    def get_word(self, ix):
        return self.from_ix(ix)

    def get_ix(self, word):
        """
        :param word: string
        :return: the corresponding index if the word is in the vocab, the corresponding base index if the word matches
         an odd case, the <UNKNOWN> index if the word is unknown and <UNKNOWN> exists, error otherwise
        """
        if word in self.to_ix:
            return self.to_ix[word]
        else:
            # check if word matches a template
            nb_matches = 0
            for template, base in self.odd_cases.items():
                if re.fullmatch(template, word):
                    nb_matches += 1
                    match = base
            if nb_matches > 1:
                raise ValueError("{} matches several templates".format(word))
            if nb_matches == 1:
                return self.to_ix[base]
            else:
                try:
                    return self.to_ix['<UNKNOWN>']  # word is unknown
                except KeyError:
                    raise KeyError("{} is unknown and the vocabulary has no <UNKNOWN> word. Add one to handle such cases".format(word))

    def seq_to_ix(self, seq):
        """
        :param seq: list of words
        :return: LongTensor of indexes of these words (unknown words are matched to <UNKNOWN> i.e 0)
        """
        ix_list = [self.get_ix(word) for word in seq]
        return torch.LongTensor(ix_list)

    def seq_from_ix(self, seq):
        """
        :param seq: list of indexes
        :return: list of words
        """
        return [self.from_ix[ix] for ix in seq]

    def from_json(self, path):
        """
        loads a pre-existing vocab from a .json file(current vocab is destroyed)
        :param path: input path
        """
        with open(path, 'r', encoding='utf-8-sig') as f:
            dict = json.load(f)
        self.__dict__ = dict

    def to_json(self, path):
        """
        writes the vocab to a .json file
        :param path: output path
        """
        with open(path, 'w+', encoding='utf-8-sig') as f:
            json.dump(self.__dict__, f)


class ArticleDataset(Dataset):
    """
    stores a list of tagged texts and the corresponding vocab with convenient methods
    """
    def __init__(self, articles = None, word_voc=None, tag_voc=None, char_voc=None):
        """
        :param articles: list of tagged articles, IOB format : [(tokens, tags), (tokens, tags), ...] with tokens being [word1, word2, ...], same for tags
        :param word_voc: word Vocab
        :param tag_voc: tag Vocab
        """
        self.articles = articles if articles else []
        self.word_voc = word_voc if word_voc else Vocab(['<UNKNOWN>'])
        self.word_voc.add_odd_case('\d+', 'NUMBER')
        self.tag_voc = tag_voc if tag_voc else Vocab()
        self.char_voc = char_voc if char_voc else Vocab(['<PAD>', '<UNKNOWN>'])


    def __len__(self):
        return len(self.articles)

    def __getitem__(self, item):
        """
        :param item: int
        :return: the i-th article as a tuple of index LongTensor sequence, labels (network input)
        """
        words, tags = self.articles[item]
        return self.word_voc.seq_to_ix([word.lower() for word in words]), self.tag_voc.seq_to_ix(tags)

    def remove_empty(self):
        for i in range(len(self)):
            words, tags = self.articles[i]
            empty_ix = []
            for j in range(len(words)):
                if words[j] == '':
                    empty_ix.append(j)
            words = list(np.delete(words, empty_ix))
            tags = list(np.delete(tags, empty_ix))
            self.articles[i] = words, tags

    def shuffle(self):
        """"Creates a dataset with the same articles that have been shuffled.
        This function should be called between each epoch.
        :return: new dataset shuffled"""
        shuffled_ix = list(range(len(self)))
        random.shuffle(shuffled_ix)
        shuffled_articles = [self.articles[ix] for ix in shuffled_ix]
        new_dataset = ArticleDataset(articles=shuffled_articles, word_voc=self.word_voc, tag_voc=self.tag_voc, char_voc=self.char_voc)
        print("shuffled")
        return new_dataset

    def batch_split(self, batch_size=32):
        """"Splits the dataset in mini-batches of a given size. The last batch is smaller or equal than the batch size
        :param batch_size: int, size of a batch
        :return: list of datasets of given size """
        n = len(self)
        minibatch_idx = [list(range(i*batch_size, min((i+1)*batch_size, n))) for i in range(-int(-n//batch_size))]
        minibatch = []
        for list_idx in minibatch_idx:
            articles = [self.articles[ix] for ix in list_idx]
            minibatch.append(ArticleDataset(articles=articles, word_voc=self.word_voc, tag_voc=self.tag_voc, char_voc=self.char_voc))
        return minibatch

    def split(self, ratio=0.8):
        """
        Splits self into a train and a test.txt dataset, with ratio in train and 1-ratio in test.txt
        :param ratio: float
        :return: the test.txt and train datasets
        """
        train_ix = random.sample(list(range(len(self))), k=int(ratio*len(self)))
        test_ix = [i for i in range(len(self)) if i not in train_ix]
        train_articles = [self.articles[ix] for ix in train_ix]
        test_articles = [self.articles[ix] for ix in test_ix]
        train_data = ArticleDataset(articles=train_articles, word_voc=self.word_voc, tag_voc=self.tag_voc, char_voc=self.char_voc)
        test_data = ArticleDataset(articles=test_articles, word_voc=self.word_voc, tag_voc=self.tag_voc, char_voc=self.char_voc)
        return train_data, test_data

    def cross_validation(self, k = 5):
        """
        Splits self into k (train dataset, test.txt dataset)
        :param k: int, number of folds
        :return: k-list of [train dataset, test.txt dataset]
        """
        cv_list = []
        shuffled_list = list(range(len(self)))
        random.shuffle(shuffled_list)
        list_test_ix = [shuffled_list[i::k] for i in range(k)]
        for i in range(k):
            test_ix = list_test_ix[i]
            train_ix = [i for i in range(len(self)) if i not in test_ix]
            train_articles = [self.articles[ix] for ix in train_ix]
            test_articles = [self.articles[ix] for ix in test_ix]
            train_data = ArticleDataset(articles=train_articles, word_voc=self.word_voc, tag_voc=self.tag_voc, char_voc=self.char_voc)
            test_data = ArticleDataset(articles=test_articles, word_voc=self.word_voc, tag_voc=self.tag_voc, char_voc=self.char_voc)
            cv_list.append((train_data, test_data))
        return cv_list

    def to_iob_json(self, path, add=False):
        """
        write the articles into a .json file, IOB-formatted
        :param path: path of the output file
        :param add: if True, will add the articles to the file without rewriting it
        """
        mode = 'a' if add else 'w+'
        with open(path, mode) as f:
            lines = ['\n'] if add else []
            for tokens, tags in self.articles:
                lines.append(json.dumps({'sequence': tokens, 'tags': tags})+'\n')
            lines[-1] = lines[-1][:-1] #remove the last \n
            for line in lines:
                f.write(line)

    def update_vocs(self, trim=-1):
        """
        update self.word_voc and self.tag_voc with the words encountered in the articles
        """
        for words, tags in self.articles:
            self.word_voc.add_text([word.lower() for word in words])
            self.tag_voc.add_text(tags)
            for word in words:
                self.char_voc.add_text(word)
        if trim > 0:
            print("trim")
            self.word_voc.trim(threshold=trim, exclude=['<UNKNOWN>'])
            self.char_voc.trim(threshold=trim, exclude=['<PAD>', '<UNKNOWN>'])

    def from_iob_json(self, path):
        """
        loads articles from a .json file, IOB-formatted
        :param path: path of the input file
        """
        with open(path, 'r', encoding='utf-8-sig') as f:
            for line in f:
                sample = json.loads(line)
                self.articles.append((sample['sequence'], sample['tags']))

    def from_offsets_json(self, path):
        """
        loads articles from a .json file, offsets-formatted (Doccano export format)
        :param path:
        :return:
        """
        with open(path, 'r', encoding = 'utf-8-sig') as file:
            for i, line in enumerate(file):
                    #load and convert the json file to IOB
                    article = json.loads(line)
                    tokens, tags = zip(*offsets2iob(article))
                    self.articles.append((tokens, tags))

    def to_ascii(self):
        self.articles = [([unicodeToAscii(word) for word in text], labels) for text, labels in self.articles]


class CharDataset(Dataset):
    """
    stores a text with ways to split it in sequences -- not very used, probably should be deleted and recoded if needed
    """
    def __init__(self, file = None, char_voc = Vocab()):
        self.text = ''
        if file is not None:
            with open(file, 'r', encoding='utf-8-sig') as f:
                self.text = f.read()
        self.char_voc = char_voc
        self.sequences = []

    def build_sequences(self, method='cut', max_length=100):
        n = len(self.text)
        if method == 'cut':
            for i in range(0, n, max_length):
                self.sequences.append(self.text[i: min(i+max_length, n)])
        if method == 'words':
            self.sequences = self.text.split()

    def __getitem__(self, item):
        return self.char_voc.seq_to_ix(self.sequences[item])

    def __len__(self):
        return len(self.sequences)

    def update_voc(self):
        self.char_voc.add_text(self.text) # when text is not a list of string but a string, add_text() will add the chars

    def to_file(self, path, add=False):
        mode = 'a' if add else 'w+'
        with open(path, mode, encoding = 'utf8') as f:
            f.write(self.text)

    def from_dir(self, path):
        for filename in os.listdir(path):
            with open(os.path.join(path, filename), 'r', encoding='utf-8-sig') as f:
                text = f.read()
            self.text += text
            self.sequences.append(text)

    def add_sequences(self, sequences):
        self.text += ' '.join(sequences)
        self.sequences.extend(sequences)

    def split(self, ratio=0.8):
        train_ix = random.sample(list(range(len(self))), k=int(ratio * len(self)))
        test_ix = [i for i in range(len(self)) if i not in train_ix]
        train_sequences = [self.sequences[ix] for ix in train_ix]
        test_sequences = [self.sequences[ix] for ix in test_ix]
        train_data = CharDataset(char_voc=self.char_voc)
        test_data = CharDataset(char_voc=self.char_voc)
        train_data.add_sequences(train_sequences)
        test_data.add_sequences(test_sequences)
        return train_data, test_data

    def to_ascii(self):
        self.text = unicodeToAscii(self.text)
        self.sequences = [unicodeToAscii(seq) for seq in self.sequences]


def tokenize(sentence):
    """
    splits a sentence in words and punctuation items
    :param sentence: string
    :returns text: list of tokens
    """
    in_word = False  # whether or not the currently read char is in a word or space/punctuation
    cur_word = ""  # word being written
    text = []
    for charcount, char in enumerate(sentence):
        prev_in_word = in_word
        in_word = re.match('[\w\-]', char)  # true if char is a...z, A...Z, 0...9 or '-'
        if in_word:     # inside a word
            cur_word += char
        elif prev_in_word:  # at the end of a word
            text.append(cur_word)
            cur_word = ""
        if char in ".,:;?!()\"'\n":     # special character
            text.append(char)
    return text

