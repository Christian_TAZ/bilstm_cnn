# coding=utf-8
from __future__ import print_function
import optparse
import itertools
from collections import OrderedDict
import loader
import torch
import time
import _pickle as cPickle
from torch.autograd import Variable
import matplotlib.pyplot as plt
import sys
from utils import *
from loader import *
from model import BiLSTM_CRF
from data_preprocessing import Vocab, ArticleDataset
t = time.time()
models_path = "models/"

optparser = optparse.OptionParser()
optparser.add_option(
    "-T", "--train", default="dataset/test3_small.json",
    help="Train set location"
)

optparser.add_option('--word_vocab_path', type=str, default=os.path.join('dataset', 'word_voc.json'), help='path to word vocabulary')
optparser.add_option('--char_vocab_path', type=str, default=os.path.join('dataset', 'char_voc.json'), help='path to char vocabulary')
optparser.add_option('--tag_vocab_path', type=str, default=os.path.join('dataset', 'tag_voc.json'), help='path to tag vocabulary')

opts = optparser.parse_args()[0]



word_voc = Vocab(['<UNKNOWN>'])
tag_voc = Vocab()
char_voc = Vocab(['<PAD>', '<UNKNOWN>'])
word_voc.from_json(opts.word_vocab_path)
tag_voc.from_json(opts.tag_vocab_path)
char_voc.from_json(opts.char_vocab_path)


dataset = ArticleDataset(word_voc=word_voc, tag_voc=tag_voc, char_voc=char_voc)
dataset.from_iob_json(opts.train)
dataset.remove_empty()
print('Test dataset loaded : ' + str(len(dataset)) + ' articles')


train_sentences=[]
for sentence, tags in dataset.articles:
    for  s, t in zip(sentence, tags):
            train_sentences.append(s+' '+t)
    train_sentences.append("\n")

fichier = open("dataset/test3_small.txt", "a")
for t in train_sentences:
    fichier.write("\n")
    if t != "\n":
     fichier.write(t)
fichier.close()

# f1=open("train1.txt", "a")
# f2=open("train2.txt", "a")

# def split(train):
#     split= int(len(train)*0.9)
#     for counter, t in enumerate(train):
#         if counter <= split:
#             f1.write("\n")
#             if t != "\n":
#                 f1.write(t)
#         else:
#             f2.write("\n")
#             if t != "\n":
#                 f2.write(t)
#     f1.close()
#     f2.close()
#
# split(train_sentences)