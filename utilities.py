# coding: utf8
import json
import os
import re
import string
import unicodedata

import spacy
import spacy.gold





def iob2spacybiluo(doc):
    """
    :param doc: expected to be under the following format:
    "Word IOB_TAG
    word IOB_TAG
    word IOB_TAG

    Word IOB_TAG
    word IOB_TAG
    ..."

    (1 line = 1 word and its IOB tag, with an empty line indicating the end of a sentence

    :returns a tree-like structure:
    -doc
        -paragraph
            -sentence
                -token : {"orth": word, "tag": pos, "ner": biluo}
                -token
            -sentence

    Each element is a dictionnary containing (optional) info about itself and a list of its children, ie a 'doc' element
    is of the form {'id': int, 'paragraphs': [list of paragraphs]}
    """
    sentences = re.split('\n\n', doc)
    paragraph = {'sentences': []}
    for sentence in sentences:
        tokens = sentence.split('\n')
        words, iob = zip(*[token.split() for token in tokens])
        pos = ['-'] * len(words)  # in the future we might support file containing PoS tags too
        biluo = spacy.gold.iob_to_biluo(iob)  # spacy wants biluo, not iob
        tokens = [{'orth': word, 'tag': p, 'ner': ent} for word, p, ent in zip(words, pos, biluo)]
        paragraph['sentences'].append({'tokens': tokens})
    doc = {'id': 0, "paragraphs": [paragraph]}
    return [doc]


def iob2spacyjson(inpath, outpath):
    """
    :param inpath: path of a text file (IOB tagged, see before)
           outpath: path of the target .json file
    :return: nothing
    """
    with open(inpath, 'r', encoding='utf8') as infile:
        doc = infile.read()
        tree_doc = iob2spacybiluo(doc)
    with open(outpath, 'w', encoding='utf8') as outfile:
        json.dump(tree_doc, outfile)


def offsets2iob(sentence):
    """
    :param sentence: a dictionnary {'text': str, 'labels':[[start, end, label], ...]}
    :return: a IOB-formatted list [[token, IOB-tag], [token, IOB-tag],...]
    """
    labels = sorted(sentence['labels'], key=lambda lab: lab[0])
    waiting_label = 0  # which label is currently expected to be gone through
    isFirst = False  # indicates whether to put a B-tag or an I-tag
    nb_labels = len(labels)
    inWord = False  # whether or not the currently read char is in a word or space/punctuation
    cur_tag = 'O'
    cur_word = ""  # word being written
    text = []
    for charcount, char in enumerate(sentence['text']):
        prevInWord = inWord
        prev_tag = cur_tag
        inWord = re.match('[\w\-]', char)  # true if char is a...z, A...Z, 0...9 or '-'
        # next, checks if we already labelled everything or if we need to update the waiting label
        if waiting_label < nb_labels and charcount >= labels[waiting_label][1]:
            waiting_label += 1
        if waiting_label < nb_labels:
            if inWord:
                isEnt = labels[waiting_label][
                            0] <= charcount  # we are in an entity if the charcount is between the start and end of the waiting label (cannot be after the end or we would have changed label)
                isFirst = isFirst or labels[waiting_label][
                    0] == charcount  # set to true when we enter a new entity, set to false when we get out of the word
                if isEnt:
                    cur_tag = ('B' if isFirst else 'I') + '-' + labels[waiting_label][2]
                else:
                    cur_tag = 'O'
            else:
                isFirst = False
        else:
            cur_tag = 'O'
        if inWord:
            cur_word += char
        elif prevInWord:
            assert cur_word != ''
            text.append([cur_word,
                         prev_tag])  # if we just got out of a word, it's time to write the tag. prev_tag because even though a non-word char doesn't update the cur_tag, it might change if we get past the last label
            cur_word = ""
        if char in ".,:;?!()\"\"\n":
            text.append([char, 'O'])
    return text


def doccano2iob(input_path, output_path=None):
    """
    :param input_path: path to a .json file as exported by the Doccano text annotator. Strucure is the following:
        {'id': 0, 'text': sentence1, 'labels': [[start, end, label1], [start, end, label2], ...]}
        {'id': 1, 'text': sentence2, 'labels': [[start, end, label1], [start, end, label2], ...]}
    :param output_path: path to where you want to save the output
    :return: iobdoc is a string following the IOB format for named entities recognition:
        "Word TAG
         word TAG
         ..."
    """
    iobdoc = ''
    with open(input_path, 'r', encoding='utf8') as json_file:
        for line in json_file:
            sentence = json.loads(line)  # one sentence, with text and labels position
            iobsent = offsets2iob(sentence)
            iobdoc += iobsent + '\n'
    if output_path is not None:
        with open(output_path, 'w+') as iobfile:
            iobfile.write(iobdoc)
    return iobdoc


def doccano2spacy_offsets(path):
    """
    :param path: path to a .json file, doccano format:
        {'id': int, 'text': sentence1, 'labels':[[start, end, label], [start, end, label],...]}
        {...}
    :return: [('sentence1', {'entities':[(start, end, label), (start, end, label),...]}), (...)]
    """
    sentences = []
    with open(path, 'r', encoding='utf8') as jsonfile:
        for line in jsonfile:
            sentence = json.loads(line)
            text, labels = sentence['text'], sentence['labels']
            labels = [(start, end, label) for [start, end, label] in labels]
            sentence = (text, {'entities': labels})
            sentences.append(sentence)
    return sentences


def spacy_offsets2offsets(sentences):
    """
    :param sentences: [('sentence1', {'entities':[(start, end, label), (start, end, label),...]}), (...)]
    :return: [{'text': str, 'labels': [[start, end, label], ...]}, {...},...]
    """
    return [{'text': sentence, 'labels': [[start, end, label] for start, end, label in dict['entities']]} for
            sentence, dict in sentences]


def text2emptyjson(indir, outpath):
    """
    :param inpath: path of a directory containing raw text files (as created by get_article for example)
    :param outpath: path of the .json file where you want to save them
                    format if the following: {'text': article 1, 'labels':[]}
                                             {'text': article 2, 'labels': []}
                                             ...
    """
    with open(outpath, 'w+', encoding='utf8') as jsonfile:
        nb_files = len(os.listdir(indir))
        for i, file in enumerate(os.listdir(indir)):
            with open(os.path.join(indir, file), 'r', encoding='utf-8-sig') as textfile:
                raw = textfile.read()
                dic = {'text': raw, 'labels': []}
                json.dump(dic, jsonfile)
                if i < nb_files - 1:
                    jsonfile.write('\n')


def display_aligned(*seqs):
    lengths = [[len(w) for w in seq] for seq in seqs]
    max_lengths = [max(ls) for ls in zip(*lengths)]
    total_length = sum(max_lengths) + len(max_lengths)
    template = ' '.join(['{' + ':<{}'.format(l) + '}' for l in max_lengths])
    print('-' * total_length)
    for seq in seqs:
        print(template.format(*seq))


def display_progress(progress, bar_length=20, start='', circle=True):
    # progress must be between 0 and 1
    block = int(round(bar_length * progress))
    # circle loading bar
    if circle:
        list_circle = ['\u25EF', '\u25D4', '\u25D1', '\u25D5', '\u25CF']
        print('\r' + start + ' {0} {1:.2f}%'.format(list_circle[int(round(progress * 4))], progress * 100), end='')
    else:
        div, rest = bar_length // 4, bar_length % 4
        if rest == 0:
            full_bar = '\u2591' * div + '\u2592' * div + '\u2593' * div + '\u2588' * div
        elif rest == 1:
            full_bar = '\u2591' * (div + 1) + '\u2592' * div + '\u2593' * div + '\u2588' * div
        elif rest == 2:
            full_bar = '\u2591' * (div + 1) + '\u2592' * div + '\u2593' * div + '\u2588' * (div + 1)
        else:
            full_bar = '\u2591' * (div + 1) + '\u2592' * (div + 1) + '\u2593' * (div + 1) + '\u2588' * div
        print(
            '\r' + start + " [{0}] {1:.2f}%".format(full_bar[:block] + "\u25AF" * (bar_length - block), progress * 100),
            end='')
    if progress == 1:
        print('')


def display_time(delta):
    if delta // (3600 * 24) > 0:
        return '{}d {}h {}m {}s'.format(int(delta // (24 * 3600)), int((delta % (24 * 3600)) // 3600),
                                        int((delta % 3600) // 60), int((delta % 60)))
    elif delta // 3600 > 0:
        return '{}h {}m {}s'.format(int((delta % (24 * 3600)) // 3600), int((delta % 3600) // 60), int((delta % 60)))
    elif delta // 60 > 0:
        return '{}m {}s'.format(int((delta % 3600) // 60), int((delta % 60)))
    else:
        return '{}s'.format(int((delta % 60)))


all_letters = string.printable
n_letters = len(all_letters)


# Turn a Unicode string to plain ASCII, thanks to https://stackoverflow.com/a/518232/2809427
def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters
    )


def iob2table(article, tags):
    assert len(article) == len(tags)
    table = {}
    for i in range(len(article)):
        if tags[i][0] == 'B':
            cur_tag = tags[i][2:]
            try:
                table[cur_tag].append(article[i])
            except KeyError:
                table[cur_tag] = [article[i]]
        if tags[i][0] == 'I':
            cur_tag = tags[i][2:]
            try:
                table[cur_tag][-1] += ' ' + article[i]
            except KeyError:
                table[cur_tag] = [article[i]]
    return table
